define [
  'beaver-console'
  'jquery'
  'jquery-plugins'
  'underscore'
  'underscore.string'
  'backbone'
  'backbone.localStorage'
  'bootstrap'
  'bootstrap-hover-dropdown'
  'typeahead'
], (jconsole) ->
  jconsole.info "lib-pack"
  _.mixin _.str.exports()
  return
