define [
  "beaver-console"
  "views/app-view"
  "controllers/socket-ctrl"
],(
  jconsole
  AppView
  jSocketCtrl
)->

  initialize:()->
    sLogHeader = "[app].initialize()"
    jconsole.info "#{sLogHeader}"

    @jSocketCtrl = jSocketCtrl

    @jAppView    = new AppView()