define [
  "beaver-console"
  "jade!templates/app-view-tpl"
  "views/navbar-view"
  "controllers/socket-ctrl"
],(
  jconsole
  AppViewTpl
  NavbarView
  jSocketCtrl
)->

  sLogHeader = "[views/app-view]"
  jconsole.group "#{sLogHeader}"

  AppView = Backbone.View.extend {
    el : 'body'
    template : AppViewTpl

    initialize : ()->
      @render()

      @listenTo jSocketCtrl, "socket:data", @onSocketData

    onSocketData : (hData)->
#      jconsole.log "#{sLogHeader}.onSocketData()"
      @updateStats hData

    render: ()->
      jconsole.log "#{sLogHeader}.render()"

      jNavbarView = new NavbarView()

      @$el
       .empty()
       .html jNavbarView.el + @template()

      return this

    updateStats: (hData)->
      @$("#rss").text hData.rss
      @$("#heapTotal").text hData.heapTotal
      @$("#heapUsed").text hData.heapUsed
  }

  jconsole.info "loaded"
  jconsole.group_end()

  return AppView