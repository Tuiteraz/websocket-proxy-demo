define [
  "beaver-console"
],(
  jconsole
)->

  sLogHeader = "[controllers/socket-ctrl]"
  jconsole.group "#{sLogHeader}"

  jCtrl = _.extend {},Backbone.Events

  sHost = window.document.location.host.replace /:.*/,""

  jCtrl.jSocket = new WebSocket "ws://#{sHost}:3335"

  jCtrl.jSocket.onmessage = (jEvent)->
#    jconsole.log "#{sLogHeader}.jSocket.onmessage()"
    jCtrl.trigger "socket:data", $.parseJSON(jEvent.data)

  jconsole.info "loaded"
  jconsole.group_end()


  return jCtrl

