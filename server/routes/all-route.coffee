#--( DEPENDENCIES

log          = require("helpers/winston-wrapper")(module)
config       = require "nconf"
requireTree  = require 'require-tree'

controllers    = requireTree '../controllers'

#--) DEPENDENCIES

module.exports = ()->
  @get "/",                   controllers.render 'index'

#  @io.route "twitter", {
#    "query" : controllers.twitter.stream.onNewQuery
#  }
  # error handling
  # BUG - if enabled prevents public resources to serve -
#  @get  "*",         controllers.render '404'

