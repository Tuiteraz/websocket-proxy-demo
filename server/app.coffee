#--( DEPENDENCIES
log          = require("helpers/winston-wrapper")(module)
config       = require "nconf"
express      = require "express"
bootable     = require "bootable"
bootableEnv  = require "bootable-environment"
http         = require "http"
_            = require "underscore"

WebSocketServer  = require("ws").Server

#--) DEPENDENCIES

#--- BODY --------------

app = bootable express()

app.phase bootable.initializers('setup/initializers/')
app.phase bootableEnv('setup/environments/',app)
app.phase bootable.routes('routes/all-route.js',app)

app.boot (err)->
  throw err if err

  server = http.createServer app

  server.listen config.get('express:port'), ()->
    log.info "Express listen port", config.get("express:port")

    sLogHeader = "[ws]"
    wss = new WebSocketServer {server : server}
    wss.on "connection", (ws)->
      iIntId = setInterval( ()->
        ws.send JSON.stringify(process.memoryUsage()), (hErr)->
          log.error "#{sLogHeader}.send() #{JSON.stringify(hErr)}" if !_.isEmpty hErr
      , 100)
      log.info "ws: started client interval"

      ws.on "end", ()->
        log.info "ws: stopping client interval"
        clearInterval iIntId


    GLOBAL.wss = wss
