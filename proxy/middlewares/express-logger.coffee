#--( DEPENDENCIES
log            = require('helpers/winston-wrapper')(module)
colors         = require('colors')
#--) DEPENDENCIES

module.exports = (jReq,jRes, fnNext)->
  log.info "[#{jReq.method.grey}]:", jReq.url
  log.info "[headers]:", JSON.stringify(jReq.headers)
  log.info("[body]:", JSON.stringify(jReq.body)) if jReq.body

  fnNext()