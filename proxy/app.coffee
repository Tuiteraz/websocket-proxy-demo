#--( DEPENDENCIES
log          = require("helpers/winston-wrapper")(module)
config       = require "nconf"
express      = require "express"
path         = require "path"

httpProxy  = require "node-http-proxy"
http       = require "http"
app        = require("express")({
  "trust proxy" : true
})

#--) DEPENDENCIES

#--- BODY --------------



config.file {"file": path.join(process.cwd(),"config.json")}

app.use express.errorHandler()
server = http.createServer app

iPort = config.get('express:port')
sRemoteHostIP = config.get('ws:remoteHostIP')

log.info "creating proxy server..."
proxy= httpProxy.createProxyServer({
  target : "http://#{sRemoteHostIP}:#{iPort}"
  ws  : true
})

proxy.on "error", (jError)->
  log.error "#{JSON.stringify(jError)}"


app.all '*', (jReq,jRes)->
  log.info "proxying : #{jReq.url}"
  proxy.web jReq, jRes

server.on "upgrade", (jReq,jSocket,hHead)->
  log.info "proxying socket : #{jReq.url}"
  proxy.ws jReq,jSocket, hHead


log.info "Starting proxy server : localhost:80->#{iPort} -> #{sRemoteHostIP}:#{iPort}"
server.listen iPort